# Elypia Emotes [![Discord][discord-members]][discord] [![GitLab Build][gitlab-build]][gitlab]
All emotes for Elypia which are open-source and available via the Apache 2.0 license.  
For information on [contributing](CONTRIBUTING.md) please check out that file. 

Currently all emotes are being redone in HD and with an image mask for better quality and to allow multiple colors! 

## Download
You can download all emotes [here][download]!  

---

Sometimes GitLab Pages isn't up to date with the repo, if there's an emote you want but you can't find it in the archive, feel free to [download][download-latest-pipeline] the artifacts from the latest pipeline instead, it should be available there!

## Emotes
### All Colors
![All Colors][colors]

### All Emotes
![All Emotes][emotes]

[discord]: https://discord.gg/hprGMaM "Discord Invite"
[discord-members]: https://discordapp.com/api/guilds/184657525990359041/widget.png "Discord Shield"
[gitlab]: https://gitlab.com/Elypia/elypia-emotes/commits/master "Repository on GitLab"
[gitlab-build]: https://gitlab.com/Elypia/elypia-emotes/badges/master/pipeline.svg "GitLab Build Shield"
[download-latest-pipeline]: https://gitlab.com/Elypia/elypia-emotes/-/jobs/artifacts/master/download?job=pages "Download Latest Pipeline"
[download]: https://elypia.gitlab.io/elypia-emotes/emotes.zip "All Emotes Packaged"
[colors]: https://elypia.gitlab.io/elypia-emotes/colors.png "All Colors"
[emotes]: https://elypia.gitlab.io/elypia-emotes/emotes.png "All Unique Emotes"